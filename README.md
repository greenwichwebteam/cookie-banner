# Greenwich cookies banner #

A cookie banner for Royal Borough of Greenwich websites that loads in google analytics if the user agrees to analytics cookies.

This cookie banner is built in JavaScript, so should be compatible with most websites.

## Implementation ##

Place this project somewhere on the server, accessible to web traffic.

The following code should be in the site template, so that it can be generated on every page on the site.

Place the following script tag:
```
<script src="/path/to/cookie-banner/include.js"></script>
```

Next, call a new Banner object and call the execute() method. Configuration of the banner can be passed through to the object parameter as an object or JSON, like so:
```
<script>
	let config = {}
	config.path = "/site/custom_scripts/apps/cookie-banner"
	config.GAID = "UA-28855068-1"
	config.domain = "royalgreenwich.gov.uk"
	let cb = new Banner(config)
	cb.execute()
</script>
```

or, it can be configured via the object properties:
```
<script>
	let cb = new Banner()
	cb.path = "/site/custom_scripts/apps/cookie-banner"
	cb.GAID = "UA-28855068-1"
	cb.domain = "royalgreenwich.gov.uk"
	cb.execute()
</script>
```

The full list of config variables are described in the table below.

Variable name			| Value
------------------------|------------------------
path					| The web path to this project directory, e.g. /libs/cookie-banner
GAID					| Your Google Analytics ID
domain					| The domain used for setting acceptance cookies, a.k.a. your website domain. If the website is using a subdomain of royalgreenwich.gov.uk, use royalgreenwich.gov.uk
preferences_uri			| (Optional) An array of URIs within which the cookie banner will not display, and if preferences_selector has been set, a preferences form will be loaded
preferences_selector	| (Optional) Will generate a form at the URIs mentioned in **preferences_uri**, allowing the user to enable/disable 
link_selector			| (Optional, recommended) A CSS-style selector within which **link_html** will be placed on every page, providing a way to display the cookie banner.
link_html				| (Optional, recommended) HTML including a button with the id 'cookie-banner-button', providing a way to display the cookie banner
heading                 | (Optional) HTML for the cookie banner heading. Defaults to '<h2>Cookie preferences</h2>'.
content                 | (Optional, recommended) HTML for the cookie banner content. Recommended to customise this, the default content links back to https://www.royalgreenwich.gov.uk/cookies
type					| (Optional) Allows for other cookie categories to be defined, as well as descriptions for the preferences form and an array of scripts to be loaded in if accepted. e.g. `type.tracking = {}; type.tracking.scripts = ['/tracking.js']; type.tracking.content = "Tracking cookies enable us to..."`


Finally, add the following noscript tag, for users who may have JavaScript disabled:
```
<noscript class='cookie-banner'>We use necessary cookies to make our site work. For more detailed information about the cookies we use, see our <a href='/cookies'>cookies page</a>.</noscript>
```



var domain = document.currentScript.getAttribute('data-domain')
var preferences_uri = JSON.parse(document.currentScript.getAttribute('data-pref-uris'))
var preferences_selector = document.currentScript.getAttribute('data-pref-selector')
var link_selector = document.currentScript.getAttribute('data-link-selector')
var link_html = document.currentScript.getAttribute('data-link-html')
var heading_html = document.currentScript.getAttribute('data-heading-html')
var content_html = document.currentScript.getAttribute('data-content-html')
var cookie_types = JSON.parse(document.currentScript.getAttribute('data-cookie-types'))
var tabindex = document.currentScript.getAttribute('data-tabindex')

var cookie_type_names = []
var cookie_entries = Object.entries(cookie_types)
cookie_entries.forEach(function(v, i) {
	cookie_type_names.push(v[0])
})


/**
 * Wait for an element to exist, then return it
 */
function wait_for_element(selector) {
	var element = document.querySelector(selector)
	if (element!=null) {
		return element
	} else {
		return setTimeout(function() {
			console.log("Element '"+selector+"' was missing. Retrying...")
			return wait_for_element(selector)
		}, 1000)
	}
}

var cookies_allowed = true
document.cookie = "CookieBannerTest=true; path=/; SameSite=Lax; domain="+domain
if (getCookie("CookieBannerTest")!="true") {
	console.warn("Cookies blocked. Banner will not be raised.")
	cookies_allowed = false
}
document.cookie = "CookieBannerTest=true; expires=Thu, 18 Jan 1970 12:00:00 UTC; path=/; SameSite=Lax; domain="+domain

if (preferences_uri.indexOf(window.location.pathname)=="-1") {
	// Display cookie banner
	if (cookies_allowed==true) {
		sessionStorage.setItem("cookie-banner-url", window.location.pathname)
		if (link_selector!="") {
			wait_for_element(link_selector).innerHTML+= link_html
			document.querySelector("#cookie-banner-button").onclick = function() {load_banner(false)}
		}
		if (getCookie("rbg_essential_cookies")!="true") {
			load_banner(true)
		}
	}
} else {
	// Generate form
	if (cookies_allowed==true) {
		load_preferences_form()
	} else {
		load_cookies_disallowed_message()
	}
}

function load_preferences_form() {
	if (preferences_selector=="") {
		return false
	}

	var analytics_yes = ""
	var analytics_no = ""

	// Look at existing cookies for checkbox presets
	if (getCookie("rbg_analytics_cookies")=="true") {
		analytics_yes = "checked"
		analytics_no = ""
	} else {
		analytics_yes = ""
		analytics_no = "checked"
	}

	var settings_content = ""
	settings_content+= "<form id='cookie-preferences'>"
	settings_content+= "<fieldset class='disabled'>"
	settings_content+= "<legend>Strictly necessary cookies (required)</legend>"
	settings_content+= "<p>Necessary cookies enable core functionality such as security, network management, and accessibility. You may disable these by changing your browser settings, but this may affect how the website functions.</p>"
	settings_content+= "<label>On<input type='radio' name='necessary-cookies' value='On' disabled checked='checked' /></label>" 
	settings_content+= "<label>Off<input type='radio' name='necessary-cookies' value='Off' disabled/></label>"
	settings_content+= "</fieldset>"

	cookie_entries.forEach(function(v, i) {
		// Look at existing cookies for checkbox presets
		preselect_on = ""
		preselect_off = ""
		if (v[1].force=="on" || (v[1].force==undefined && getCookie("rbg_"+v[0]+"_cookies")=="true")) {
			preselect_on = "checked='checked'"
			preselect_off = ""
		} else if (v[1].force=="off" || (v[1].force==undefined && getCookie("rbg_"+v[0]+"_cookies")=="")) {
			preselect_on = ""
			preselect_off = "checked='checked'"
		}
		let disabled = (v[1].force!=undefined?"disabled=disabled":"")
		let fieldset_class = (v[1].force!=undefined?"disabled":"")
		settings_content+= "<fieldset class='"+fieldset_class+"'>"
		settings_content+= "<legend>"+v[0].replace(/^\w/, (c) => c.toUpperCase())+" cookies</legend>"
		settings_content+= (v[1].content!=undefined?v[1].content:"")
		settings_content+= "<label>On<input type='radio' id='"+v[0]+"-acceptance' name='"+v[0]+"-cookies' value='On' "+preselect_on+" "+disabled+"/></label>"
		settings_content+= "<label>Off<input type='radio' name='"+v[0]+"-cookies' value='Off' "+preselect_off+" "+disabled+"/></label>"
		settings_content+= "</fieldset>"
	})

	settings_content+= "<button type='submit'>Save preferences</button>"
	settings_content+= "</form>"
	wait_for_element(preferences_selector).innerHTML+= settings_content
	document.querySelector("form#cookie-preferences").onsubmit = function(e) {
		e.preventDefault()
		var accepted = []
		cookie_type_names.forEach(function(v) {
			if (document.querySelector("#"+v+"-acceptance").checked==true) {
				accepted.push(v)
			}
		})
		set_acceptance_cookies(accepted)
		if (document.querySelector("#preferences-set-message")==null) {
			var new_node = document.createElement("div")
			new_node.setAttribute("id","preferences-set-message")
			document.querySelector("div#cookies_settings").prepend(new_node)
		}
		document.querySelector("#preferences-set-message").innerHTML = "<p>Preferences set. <a href='"+sessionStorage.getItem("cookie-banner-url")+"'>Return to the page you were on</a>.</p>"
	
		return false
	}
}

function load_cookies_disallowed_message() {
	if (preferences_selector=="") {
		return false
	}
	var settings_content = "Your browser is currently not accepting cookies. Some aspects of our site may not work as intended."
	wait_for_element("#cookies_settings").innerHTML+= settings_content
}

function load_banner(first_run) {
	var analytics_checked = ""
	if (!first_run) {
		// Look at existing cookies for checkbox presets
		if (getCookie("rbg_analytics_cookies")=="true") {
			analytics_checked = "checked"
		}
	}

	var cookie_banner = document.createElement("div")
	cookie_banner.id = "cookie-banner"
	cookie_banner.classList.add("cookie-banner")

	var cookie_content_div = document.createElement("div")
	cookie_content_div.id = "cookie-content"

	cookie_content = heading_html
	cookie_content+= content_html
	cookie_content+= "<button id='cookies_accept_all' class='button' tabindex='"+tabindex+"'>Accept all cookies</button>"
	cookie_content+= "<button id='cookies_accept_necessary' class='button' tabindex='"+tabindex+"'>Accept necessary cookies only</button>"
	cookie_content_div.innerHTML = cookie_content
	wait_for_element("body").appendChild(cookie_banner)
	document.querySelector("#cookie-banner").appendChild(cookie_content_div)

	document.querySelector("div#cookie-banner button#cookies_accept_all").onclick = function() {
		set_acceptance_cookies(cookie_type_names)
		document.querySelector("div#cookie-banner").remove()
	}
	document.querySelector("div#cookie-banner button#cookies_accept_necessary").onclick = function() {
		set_acceptance_cookies(["essential"])
		document.querySelector("div#cookie-banner").remove()
	}
}

function set_acceptance_cookies(types) {
	var d = new Date()
	d.setMonth(d.getMonth()+3)
	var n = d.toUTCString()
	document.cookie = "rbg_essential_cookies=true; expires="+n+"; path=/; domain="+domain
	Object.entries(cookie_types).forEach(function(v) {
		if (types.indexOf(v[0])!=-1) {
			// If listed, make new cookie
			document.cookie = "rbg_"+v[0]+"_cookies=true; expires="+n+"; path=/; domain="+domain
		} else {
			// Remove cookie
			document.cookie = "rbg_"+v[0]+"_cookies=true; expires=Thu, 18 Jan 1970 12:00:00 UTC; path=/; domain="+domain
		}
	})
}

function getCookie(cname) {
  var name = cname + "="
  var decodedCookie = decodeURIComponent(document.cookie)
  var ca = decodedCookie.split(';')
  for (var i = 0; i <ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ""
}


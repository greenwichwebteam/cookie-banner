// Get the search parameters from the script src
var path = "?"+document.currentScript.getAttribute('src').split('?')[1]

var urlParams = new URLSearchParams(path)
var GAID = urlParams.get('gaid')

window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', GAID);

